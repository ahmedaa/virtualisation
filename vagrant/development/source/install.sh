#!/bin/bash

function fonts() {
  # FantasqueSansMono
  wget -P /tmp https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FantasqueSansMono.zip && unzip /tmp/FantasqueSansMono.zip -d /usr/share/fonts/truetype
}

function ohmyzsh() {
  colorecho "Installing oh-my-zsh, config, history, aliases"
  su -c "git clone https://github.com/ohmyzsh/ohmyzsh $USER_HOME/.oh-my-zsh" $USER
  su -c "cp -v $BASE_DIR/source/shell/zshrc $USER_HOME/.zshrc" $USER
  su -c "git -C $USER_HOME/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-autosuggestions" $USER
  su -c "git -C $USER_HOME/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-syntax-highlighting" $USER
  su -c "git -C $USER_HOME/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-completions" $USER
  su -c "git -C $USER_HOME/.oh-my-zsh/custom/plugins/ clone https://github.com/agkozak/zsh-z" $USER

  su -c "git clone https://github.com/denysdovhan/spaceship-prompt.git $USER_HOME/.oh-my-zsh/custom/themes/spaceship-prompt" $USER && \
    su -c "ln -s $USER_HOME/.oh-my-zsh/custom/themes/spaceship-prompt/spaceship.zsh-theme $USER_HOME/.oh-my-zsh/custom/themes/spaceship.zsh-theme" $USER
}

function locales() {
  colorecho "Configuring locales"
  apt-get -y install locales
  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
}

function tmux() {
  colorecho "Installing tmux"
  apt -y install tmux
  cp -v $BASE_DIR/source/tmux/tmux.conf $USER_HOME/.tmux.conf
  touch $USER_HOME/.hushlogin

  colorecho "Setting up tmux plugin manager"
  su -c "mkdir -p $USER_HOME/.tmux/plugins" $USER
  su -c "git -C $USER_HOME/.tmux/plugins/ clone https://github.com/tmux-plugins/tpm" $USER
  # source tmux to install plugins so they are ready?
  # have no idea how to break it off, but command below should start it in theory
  # su -c "tmux source $USER_HOME/.tmux.conf" $USER
}

function dependencies() {
  colorecho "Installing most required dependencies"
  apt-get -y install python-setuptools python3-setuptools
  pip3 install wheel
}

function fzf() {
  fapt fzf

  # Dont understand why this is needed, when it is in the repos..
  #colorecho "Installing fzf"
  #git -C /opt/tools/ clone --depth 1 https://github.com/junegunn/fzf.git
  #cd /opt/tools/fzf
  #./install --all
}

function node_version_manager() {
  colorecho "Installing node (through node version manager)"
  su -c "curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v$NVMVER/install.sh | bash" $USER
  su -c ". ${USER_HOME}/.nvm/nvm.sh; nvm install stable" $USER
}

function glow() {
  # fancy markdown reader
  colorecho "Installing Glow markdown reader"
  su -c "GOPATH=$GOPATH go get -v github.com/charmbracelet/glow; cd $GOPATH/src/github.com/charmbracelet/glow; go install" $USER
}

function spacemacs() {
  su -c "git clone -b develop https://github.com/syl20bnr/spacemacs $USER_HOME/.emacs.d" $USER
}

function install_core() {
  update || exit
  filesystem
  fonts
  ohmyzsh
  locales
  tmux
  fapt atool
  fapt automake
  fapt autoconf
  fapt autojump
  fapt bat
  fapt cmake
  fapt docker
  fapt docker.io
  fapt dos2unix
  fapt emacs
  fapt exa
  fapt flameshot
  fzf
  fapt golang
  fapt hexyl
  fapt httpie
  fapt mlocate
  fapt p7zip-full
  fapt pipenv
  fapt python3-pip
  fapt python3-venv
  fapt rar
  fapt ripgrep
  fapt unrar
  fapt virtualbox-guest-dkms
  dependencies
  glow
  node_version_manager
  spacemacs
}
