#!/bin/bash

### Bootstrapping script for new Kali VM
### REMEMBER TO REBOOT WHEN DONE!

# extras/vars #
# obviously set username to whatever user you have
USER='vagrant'
USER_HOME=/home/$USER

su -c "mkdir $USER_HOME/go" $USER
GOPATH=$USER_HOME/go

# get the working directory
BASE_DIR=$(dirname $(readlink -f $0))

# Check if versions are the latest (yes, this can be scripted, but i am lazy)
## https://github.com/nvm-sh/nvm/releases/latest
NVMVER='0.37.2'

# load functions in scope #
. $BASE_DIR/source/functions.sh

## debugging
colorecho "The username is: ${USER}"
colorecho "The users home dir is: ${USER_HOME}"
colorecho "GOPATH is: ${GOPATH}"
colorecho "The directory I am running from: ${BASE_DIR}"

###############
# environment #
###############
#   timezone  #
colorecho "Setting timezone"
timedatectl set-timezone Europe/Copenhagen

############
# software #
############
# install core #
colorecho "Installing core software and resources"
. $BASE_DIR/source/install.sh
install_core
install_resources

# Set permissions on everything we just added correctly
colorecho "Setting ownership of /home/$USER"
chown $USER:$USER -R $USER_HOME

# Change login shell to zsh - if you use bash, comment out
colorecho "Setting default shell to zsh"
chsh --shell /usr/bin/zsh $USER

# Add user to extra groups
colorecho "Adding $USER to extra groups"
gpasswd -a $USER docker

colorecho "#####################"
colorecho "#       DONE        #"
colorecho "#    SSH IN AND     #"
colorecho "#    UPGRADE GRUB   #"
colorecho "#    REMEMBER TO    #"
colorecho "#      REBOOT       #"
colorecho "#####################"
