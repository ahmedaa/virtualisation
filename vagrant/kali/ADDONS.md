# Addons in Firefox and Chrome

# Firefox

* Clear Browsing Data
* Cookie Quick Manager
* Firefox Multi-Account Containers
* FoxyProxy Standard
* RESTClient
* Wappalyzer
* Web Developer

# Chrome

* Clear Browsing Data
* EditThisCookie
* FoxyProxy Standard
* Swap My Cookies
* Wappalyzer
* Web Developer
